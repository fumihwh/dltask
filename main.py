import os
import struct
from functools import partial

import numpy as np
import sys

import time


class SoftmaxFunction:
    @staticmethod
    def forward(x):
        return np.array(list(map(lambda e: np.exp(e) / np.sum(np.exp(e), axis=0), x)))


class ReLUFunction:
    @staticmethod
    def forward(x):
        return np.maximum(x, 0)

    @staticmethod
    def backward(x, gy):
        return (x > 0) * gy


class SigmoidFunction:
    @staticmethod
    def forward(x):
        return 1.0 / (1.0 + np.exp(-1.0 * x))

    @staticmethod
    def backward(x, gy):
        t = 1.0 / (1.0 + np.exp(-1.0 * x))
        return t * (1.0 - t) * gy


class LinearFunction:
    def __init__(self, x, out_size, optimizer):
        self.x = x
        self.y = None
        self.h = None
        self.W = np.random.uniform(-1.0, 1.0, (out_size, x.shape[1]))
        self.b = np.random.uniform(-1.0, 1.0, (out_size, 1))
        self.optimizer = optimizer

    def forward(self):
        return np.dot(self.x, self.W.T) + self.b.T

    def backward(self, delta):
        gx = np.dot(delta, self.W)
        gw = np.dot(delta.T, self.x)
        gb = delta.sum(0)
        return gx, gw, gb

    def update(self, gw, gb):
        self.W = self.optimizer.run(self.W, gw)
        self.b = self.optimizer.run(self.b, gb)


class SGD:
    def __init__(self, lr=0.01):
        self.lr = lr

    def run(self, x, grad):
        x -= self.lr * (grad.reshape(x.shape))
        return x


class MNIST:
    def __init__(self, training_data, test_data, option, model):
        self.model = model
        self.batch_size = option['batch_size']
        self.n_units = option['n_units']
        self.optimizer = SGD()
        self.is_model_init = False
        self.training_data = training_data
        self.epoch = option['epoch']
        self.test_data = test_data
        self.out_units = option['out_units']

    @staticmethod
    def make1ofk(out_units, x):
        rs = np.zeros(out_units)
        rs[x[0]] = 1
        return rs

    @staticmethod
    def print_process(percentage):
        sys.stdout.write("\rLearning progress: %d%% " % percentage)
        sys.stdout.flush()
        time.sleep(0.2)

    @staticmethod
    def cross_entropy_error(h, t):
        if h.ndim == 1:
            t = t.reshape(1, t.size)
            h = h.reshape(1, h.size)
        batch_size = h.shape[0]
        return -np.sum(t * np.log(h)) / batch_size

    @staticmethod
    def accuracy(x, y, t):
        y = np.argmax(y, axis=1)
        t = np.argmax(t, axis=1)
        accuracy = {}
        num_correct = 0
        num_all = 0
        for i in range(0, 10):
            num_i_correct = len(list(filter(lambda elem: elem == i, y[t == i])))
            num_i_all = t[t == i].size
            num_correct += num_i_correct
            num_all += num_i_all
            accuracy[i] = num_i_correct / num_i_all

        accuracy['total'] = num_correct / num_all
        return accuracy

    def learn(self):
        train_indexes = list(range(0, len(self.training_data)))

        for e in range(self.epoch):
            self.print_process(round(e / self.epoch * 100))
            target_index = np.random.choice(train_indexes, self.batch_size)
            inputs = [self.training_data[i] for i in target_index]

            label = np.array(list(map(partial(self.make1ofk, self.out_units), inputs)))
            img = np.asarray(list(map(lambda x: np.reshape(x[1], x[1].size) / 255, inputs)))

            self.__core__(img, label=label)
            self.is_model_init = True
        self.print_process(100)

    def __core__(self, data, label=np.array([]), testing=False):
        # h for current layer's output and next layer's input
        # y for current layer's output without activation
        h_tmp = data.copy()
        layer_num = len(self.model)
        # forward
        for index, layer in enumerate(self.model):
            if not self.is_model_init:
                layer['layer_function'] = layer['layer_function'](h_tmp.copy(), self.n_units, self.optimizer) \
                    if index + 1 != layer_num else layer['layer_function'](h_tmp.copy(), self.out_units, self.optimizer)
            else:
                layer['layer_function'].x = h_tmp.copy()
            layer['layer_function'].y = y_tmp = layer['layer_function'].forward()
            layer['layer_function'].h = h_tmp = layer['activation'].forward(y_tmp)

        if testing:
            return h_tmp

        loss = self.cross_entropy_error(h_tmp, label)
        # backward
        delta = h_tmp - label
        # grad = test
        for index in range(len(self.model) - 1, -1, -1):
            gx, gw, gb = self.model[index]['layer_function'].backward(delta)
            self.model[index]['layer_function'].update(gw, gb)
            if index != 0:
                delta = self.model[index - 1]['activation'].backward(self.model[index - 1]['layer_function'].y, gx)

    def predict(self):
        inputs = self.test_data

        label = np.array(list(map(partial(self.make1ofk, self.out_units), inputs)))
        img = np.asarray(list(map(lambda elem: np.reshape(elem[1], elem[1].size) / 255, inputs)))

        h = self.__core__(img, label, testing=True)
        acc = self.accuracy(self.test_data, h, label)
        for x in acc:
            print(str(x) + ': ' + str(round(acc[x] * 10000) / 100) + '%')


def main():
    training_data = list(read(dataset='training', path='./data'))
    test_data = list(read(dataset='testing', path='./data'))
    model = [x for x in init_model()]
    option = {
        'batch_size': 100,
        'n_units': 1000,
        'epoch': 2000,
        'out_units': 10
    }
    mnist = MNIST(training_data, test_data, option, model)
    print('\nStart learning...')
    mnist.learn()
    print('\nStart testing...')
    mnist.predict()


def read(dataset, path):
    if dataset is "training":
        fname_img = os.path.join(path, 'train-images.idx3-ubyte')
        fname_lbl = os.path.join(path, 'train-labels.idx1-ubyte')
    elif dataset is "testing":
        fname_img = os.path.join(path, 't10k-images.idx3-ubyte')
        fname_lbl = os.path.join(path, 't10k-labels.idx1-ubyte')

    with open(fname_lbl, 'rb') as flbl:
        magic, num = struct.unpack(">2i", flbl.read(8))
        lbl = np.fromfile(flbl, dtype=np.int8)
    with open(fname_img, 'rb') as fimg:
        magic, num, rows, cols = struct.unpack(">4i", fimg.read(16))
        img = np.fromfile(fimg, dtype=np.uint8).reshape(len(lbl), rows, cols)
    get_img = lambda idx: (lbl[idx], img[idx])

    for i in range(len(lbl)):
        yield get_img(i)


def init_model():
    yield {
        'layer_function': LinearFunction,
        'activation': SigmoidFunction

    }
    yield {
        'layer_function': LinearFunction,
        'activation': SigmoidFunction

    }
    yield {
        'layer_function': LinearFunction,
        'activation': SoftmaxFunction

    }


if __name__ == '__main__':
    main()
